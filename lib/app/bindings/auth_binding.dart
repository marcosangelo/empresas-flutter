import 'package:empresas_flutter/app/controller/login/auth_controller.dart';
import 'file:///C:/Users/marco_xa3myz1/AndroidStudioProjects/empresas_flutter/lib/app/data/repository/auth/auth_repository.dart';
import 'package:get/get.dart';

class AuthBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AuthController>(() {
      return AuthController(repository: AuthRepository());
    });
  }
}
