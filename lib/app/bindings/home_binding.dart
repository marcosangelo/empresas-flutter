import 'package:empresas_flutter/app/controller/home/home_controller.dart';
import 'package:empresas_flutter/app/data/repository/home/home_repository.dart';
import 'package:get/get.dart';

class HomeBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(() {
      return HomeController(repository: HomeRepository());
    });
  }
}