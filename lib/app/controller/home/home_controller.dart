import 'package:empresas_flutter/app/data/repository/home/home_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  final HomeRepository repository;

  HomeController({@required this.repository}) : assert(repository != null);
  final _show = false.obs;

  get show => this._show.value;
}
