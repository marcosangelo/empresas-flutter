import 'package:empresas_flutter/app/data/model/investor.dart';
import 'package:empresas_flutter/app/data/model/login_auth.dart';
import 'file:///C:/Users/marco_xa3myz1/AndroidStudioProjects/empresas_flutter/lib/app/data/repository/auth/auth_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';

class AuthController extends GetxController {
  final AuthRepository repository;

  AuthController({@required this.repository}) : assert(repository != null);

  final _investor = Investor().obs;

  get investor => this._investor.value;

  set investor(value) => this._investor.value = value;

  final _loginAuth = LoginAuth().obs;

  get loginAuth => this._loginAuth.value;

  set loginAuth(value) => this._loginAuth.value = value;

  final _emailController = TextEditingController().obs;

  get emailController => this._emailController.value;

  set emailController(value) => this._emailController.value.text = value;

  final _passwordController = TextEditingController().obs;

  get passwordController => this._passwordController.value;

  set passwordController(value) => this._passwordController.value.text = value;

  final _emailFocus = new FocusNode().obs;

  get emailFocus => this._emailFocus.value;

  final _passwordFocus = new FocusNode().obs;

  get passwordFocus => this._passwordFocus.value;

  final _showPassword = false.obs;

  get showPassword => this._showPassword.value;

  set showPassword(value) => this._showPassword.value = value;

  final _loading = false.obs;

  get loading => this._loading.value;

  set loading(value) => this._loading.value = value;

  login() {
    String email = this._emailController.value.text;
    String password = this._passwordController.value.text;

    if (email.isNotEmpty && password.isNotEmpty) {
      repository.login(email: email, password: password).then((data) {
        if (data.success) {
          //TODO Redirecionar para Home

        } else {
          //TODO Tratar Errors
          print(data.toString());
        }
      }).catchError((error) {
        print(error.toString());
      });
    }
  }

  signOut() {
    repository.signOut();
  }

  goHome() {
    //TODO navegar para home quando estiver logado.
  }

  @override
  void onInit() {
    super.onInit();
    this._emailController.value.text = 'testeapple@ioasys.com.br';
    this._passwordController.value.text = '12341234';
  }
}
