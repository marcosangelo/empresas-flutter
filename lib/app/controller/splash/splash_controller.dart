import 'package:empresas_flutter/app/routes/app_pages.dart';
import 'package:empresas_flutter/app/util/constants/constants.dart';
import 'package:empresas_flutter/app/util/storage.dart';
import 'package:get/get.dart';

class SplashController extends GetxController {
  final _show = false.obs;

  get show => this._show.value;

  @override
  void onInit() {
    super.onInit();
    Future.delayed(Duration(milliseconds: 1000), () {
      this._show.value = true;
    });
    Future.delayed(Duration(seconds: 3), () {
      if (Storage().read(Constants.login_auth) != null) {
        Get.offAllNamed(Routes.HOME);
      } else {
        Get.offAllNamed(Routes.LOGIN);
      }
    });
  }
}
