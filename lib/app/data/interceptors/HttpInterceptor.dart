import 'dart:async';

import 'package:dio/dio.dart';
import 'package:empresas_flutter/app/data/model/login_auth.dart';
import 'package:empresas_flutter/app/util/constants/constants.dart';
import 'package:empresas_flutter/app/util/storage.dart';

class AppInterceptors extends Interceptor {
  @override
  Future onResponse(Response response) async {
    if (response.request.path == Constants.pathAuth) {
      if (response.headers.value("access-token") != null &&
          response.headers.value("client") != null &&
          response.headers.value("uid") != null) {
        Storage().write(
            Constants.login_auth,
            LoginAuth(
              accessToken: response.headers.value("access-token"),
              client: response.headers.value("client"),
              uid: response.headers.value("uid"),
            ).toJson());
      }
    }
    return response;
  }
}
