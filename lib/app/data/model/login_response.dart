import 'package:empresas_flutter/app/data/model/enterprise.dart';
import 'package:empresas_flutter/app/data/model/investor.dart';

class LoginResponse {
  Investor investor;
  Enterprise enterprise;
  bool success;
  List<String> errors;

  LoginResponse({this.investor, this.enterprise, this.success});

  LoginResponse.fromJson(Map<String, dynamic> json) {
    investor = json['investor'] != null
        ? new Investor.fromJson(json['investor'])
        : null;
    enterprise = json['enterprise'];
    success = json['success'];
    errors = json['errors'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.investor != null) {
      data['investor'] = this.investor.toJson();
    }
    data['enterprise'] = this.enterprise;
    data['success'] = this.success;
    data['errors'] = this.errors;
    return data;
  }
}
