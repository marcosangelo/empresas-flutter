import 'package:empresas_flutter/app/data/model/enterprise.dart';

class Portfolio {
  int enterprisesNumber;
  List<Enterprise> enterprises;

  Portfolio({this.enterprisesNumber, this.enterprises});

  Portfolio.fromJson(Map<String, dynamic> json) {
    enterprisesNumber = json['enterprises_number'];
    if (json['enterprises'] != null) {
      enterprises =  List.empty();
      json['enterprises'].forEach((v) {
        enterprises.add(new Enterprise.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['enterprises_number'] = this.enterprisesNumber;
    if (this.enterprises != null) {
      data['enterprises'] = this.enterprises.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
