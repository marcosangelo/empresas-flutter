import 'package:empresas_flutter/app/data/model/login_auth.dart';

class ResponseApi {
  int code = 0;
  bool success;
  String msg = '';
  dynamic data;
  LoginAuth loginAuth;

  ResponseApi({
    this.code,
    this.success,
    this.msg,
    this.data,
    this.loginAuth,
  });
}
