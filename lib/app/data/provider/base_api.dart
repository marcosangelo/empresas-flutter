import 'package:dio/dio.dart';
import 'package:empresas_flutter/app/data/interceptors/HttpInterceptor.dart';

class BaseApi {
  static var apiVersion = '/v1';
  static var url = 'https://empresas.ioasys.com.br/api/$apiVersion/';
  static BaseOptions options = BaseOptions(
      baseUrl: url,
      responseType: ResponseType.json,
      connectTimeout: 30000,
      receiveTimeout: 30000,
      validateStatus: (code) {
        if (code >= 200) {
          return true;
        }
        return false;
      });
  Dio _dio = new Dio(options);

  setInterceptor() {
    this._dio.interceptors.add(AppInterceptors());
  }

  Dio http() => _dio;
}
