import 'package:dio/dio.dart';
import 'package:empresas_flutter/app/data/model/login_auth.dart';
import 'package:empresas_flutter/app/data/model/login_request.dart';
import 'package:empresas_flutter/app/data/model/response_api.dart';
import 'package:empresas_flutter/app/data/provider/base_api.dart';
import 'package:empresas_flutter/app/util/constants/constants.dart';

class AuthApi extends BaseApi {
  Future<dynamic> login(LoginRequest userLoginRequest) async {
    setInterceptor();
    Response response =
        await http().post(Constants.pathAuth, data: userLoginRequest.toJson());
    if (response.statusCode >= 200 && response.statusCode <= 299) {
      return ResponseApi(
          data: LoginAuth.fromJson(response.data),
          code: response.statusCode,
          msg: response.statusMessage,
          success: true);
    } else {
      return ResponseApi(
          data: null,
          code: response.statusCode,
          msg: response.statusMessage,
          success: false);
    }
  }
}
