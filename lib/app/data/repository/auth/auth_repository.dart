import 'package:empresas_flutter/app/data/model/investor.dart';
import 'package:empresas_flutter/app/data/model/login_auth.dart';
import 'package:empresas_flutter/app/data/model/login_request.dart';
import 'package:empresas_flutter/app/data/model/response_api.dart';
import 'package:empresas_flutter/app/data/provider/login/auth_api.dart';
import 'package:empresas_flutter/app/util/constants/constants.dart';
import 'package:empresas_flutter/app/util/storage.dart';
import 'package:flutter/cupertino.dart';

class AuthRepository {
  Future<ResponseApi> login(
      {@required String email, @required String password}) {
    return AuthApi().login(LoginRequest(email: email, password: password));
  }

  saveLogin(ResponseApi responseApi) {
    Storage().write(Constants.login_auth, responseApi.loginAuth.toJson());
    Storage().write(Constants.investor, responseApi.data);
  }

  signOut() {
    Storage().remove(Constants.login_auth);
    Storage().remove(Constants.investor);
  }

  LoginAuth getLoginAuth() {
    return LoginAuth.fromJson(Storage().read(Constants.login_auth));
  }

  Investor getInvestor() {
    return Investor.fromJson(Storage().read(Constants.investor));
  }
}
