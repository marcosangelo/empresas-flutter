import 'package:empresas_flutter/app/bindings/auth_binding.dart';
import 'package:empresas_flutter/app/bindings/home_binding.dart';
import 'package:empresas_flutter/app/ui/android/home/home_page.dart';
import 'package:empresas_flutter/app/ui/android/login/login_page.dart';
import 'package:empresas_flutter/app/ui/android/splash/splash_page.dart';
import 'package:get/get.dart';

part './app_routes.dart';

class AppPages {
  static final pages = [
    GetPage(
      name: Routes.SPLASH,
      page: () => SplashPage(),
    ),
    GetPage(
      name: Routes.LOGIN,
      page: () => LoginPage(),
      binding: AuthBinding(),
    ),
    GetPage(
      name: Routes.HOME,
      page: () => HomePage(),
      binding: HomeBinding(),
    ),
  ];
}
