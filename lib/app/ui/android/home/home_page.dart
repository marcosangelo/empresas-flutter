import 'package:empresas_flutter/app/controller/home/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class HomePage extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    Widget appBar() {
      return Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('asset/image/container_home.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          clipBehavior: Clip.none,
          children: [
            Positioned(
              top: 150,
              right: 30,
              child: Container(
                padding: EdgeInsets.only(top: 0.0, left: 0.0),
                decoration: BoxDecoration(
                  color: Colors.grey[200],
                ),
                width: 350,
                child: TextField(
                  controller: new TextEditingController(text: ''),
                  obscureText: false,
                  decoration: InputDecoration(
                    icon: IconButton(
                      focusNode: null,
                      icon: Icon(
                        Icons.search,
                        color: Colors.grey,
                      ),
                      onPressed: () {},
                    ),
                    border: InputBorder.none,
                    hintText: 'Pesquise por empresa',
                  ),
                ),
              ),
            )
          ],
        ),
      );
    }

    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(150.0), // here the desired height
          child: AppBar(
            flexibleSpace: appBar(),
          )),
      body: Center(
        child: Text('HOME'),
      ),
    );
  }
}
