import 'package:empresas_flutter/app/controller/login/auth_controller.dart';
import 'package:empresas_flutter/app/ui/android/widgets/loading_widget.dart';
import 'package:empresas_flutter/app/ui/util/clip_path_class.dart';
import 'package:empresas_flutter/app/util/constants/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginPage extends GetView<AuthController> {
  @override
  Widget build(BuildContext context) {
    Widget _buildPasswordTextField() {
      return Obx(
        () => TextField(
          controller: controller.passwordController,
          focusNode: controller.passwordFocus,
          obscureText: !controller.showPassword,
          decoration: InputDecoration(
            labelText: 'Senha',
            border: OutlineInputBorder(),
            suffixIcon: IconButton(
              focusNode: null,
              icon: Icon(
                Icons.remove_red_eye,
                color: controller.showPassword ? Colors.blue : Colors.grey,
              ),
              onPressed: () {
                controller.showPassword = !controller.showPassword;
              },
            ),
          ),
        ),
      );
    }

    Widget inputFieldSection = Container(
      padding: const EdgeInsets.only(top: 20.0, left: 10.0, right: 10.0),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Obx(
                  () => TextField(
                    controller: controller.emailController,
                    focusNode: controller.emailFocus,
                    obscureText: false,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Email',
                    ),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                _buildPasswordTextField(),
              ],
            ),
          ),
        ],
      ),
    );

    Widget buttonSection = Container(
      margin: EdgeInsets.only(left: 20.0, right: 20.0, top: 40.0),
      height: 48.0,
      child: ElevatedButton(
        onPressed: () {
          controller.loading = !controller.loading;
          controller.login();
        },
        style: ElevatedButton.styleFrom(
          primary: Constants.pink,
          onPrimary: Colors.white,
          textStyle: TextStyle(fontWeight: FontWeight.bold),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [Text('ENTRAR')],
        ),
      ),
    );

    bool _keyboardIsVisible() {
      return !(MediaQuery.of(context).viewInsets.bottom == 0.0);
    }

    return Scaffold(
      body: ListView(
        children: [
          Container(
            child: ClipPath(
              clipper: ClipPathClass(),
              child: AnimatedContainer(
                width: 200.0,
                height: _keyboardIsVisible() ? 140.0 : 240.0,
                duration: Duration(milliseconds: 1000),
                curve: Curves.fastOutSlowIn,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('asset/image/container_login.png'),
                    fit: BoxFit.cover,
                  ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'asset/icon/logo_homeicon.png',
                      fit: BoxFit.cover,
                    ),
                    SizedBox(height: 20.0),
                    AnimatedOpacity(
                      duration: Duration(milliseconds: 1000),
                      opacity: _keyboardIsVisible() ? 0.0 : 1.0,
                      child: Text(
                        'Seja bem vindo ao empresas!',
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Obx(
            () => AnimatedOpacity(
              opacity: controller.loading ? 1.0 : 0.0,
              duration: Duration(milliseconds: 700),
              child: Padding(
                padding: EdgeInsets.only(top: 30.0),
                child: LoadingWidget(),
              ),
            ),
          ),
          inputFieldSection,
          buttonSection,
        ],
      ),
    );
  }
}
