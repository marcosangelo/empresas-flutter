import 'package:empresas_flutter/app/controller/splash/splash_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SplashPage extends GetView<SplashController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.maxFinite,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('asset/image/container_login.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Obx(
          () => AnimatedOpacity(
            duration: Duration(milliseconds: 1000),
            opacity: controller.show ? 1.0 : 0.0,
            child: Image.asset('asset/icon/logo_splash.png'),
          ),
        ),
      ),
    );
  }
}
