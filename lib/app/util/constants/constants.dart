import 'package:flutter/material.dart';

class Constants {
  Constants._();


  //API
  static const String pathAuth = 'users/auth/sign_in';


  static const double padding = 20;
  static const double avatarRadius = 45;

  static const Color pink = Color.fromRGBO(224, 30, 105, 1);

  //Variables do Storage
  static const String login_auth = "login_auth";
  static const String investor = "investor";
  static const String enterprise = "enterprise";
}
