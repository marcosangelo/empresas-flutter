import 'package:get_storage/get_storage.dart';

class Storage {
  final box = GetStorage();

  read(String key) {
    return box.read(key);
  }

  write(String key, Map<String, dynamic> value) {
    box.write(key, value);
  }

  remove(String key) {
    box.remove(key);
  }
}
